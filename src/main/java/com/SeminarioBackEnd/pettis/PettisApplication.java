package com.SeminarioBackEnd.pettis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PettisApplication {

    public static void main(String[] args) {
        SpringApplication.run(PettisApplication.class, args);
    }
}
